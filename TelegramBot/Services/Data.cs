﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelegramBot.Services
{
    public enum CallBackDataType
    {
        Theme = 1,
        Question = 2,
        Answer = 3
    }

    public enum CallBackRequestId
    {
        Req1 = 1, // Theme
        Req2 = 2, // Question
        Req3 = 3 // Answer
    }
    public class KeyboardData<T>
    {
        public CallBackRequestId rId { get; set; }
        public T Data { get; set; }

        public KeyboardData(CallBackRequestId rid, T data)
        {
            this.rId = rid;
            this.Data = data;
        }
        public KeyboardData()
        {

        }
    }
}
