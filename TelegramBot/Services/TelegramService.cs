﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;
using Newtonsoft.Json;
using TelegramBot.Model;
using System.Collections.Generic;
using TelegramBot.Services;

namespace TelegramBot
{
    public class TelegramService : ITelegramService
    {

        private static TelegramBotClient Bot;
        private static ApplicationContext Context;
        public TelegramBotClient _Bot { get { return Bot; } }
        public ApplicationContext _Context { get { return Context; } }
        public void SetBot(TelegramBotClient bot)
        {
            Bot = bot;
        }
        public void SetContext(ApplicationContext context)
        {
            Context = context;
        }
        public TelegramService(TelegramBotClient bot, ApplicationContext context)
        {
            Bot = bot;
            Context = context;
        }
        
        public TelegramService()
        {
        }

        public void Main()
        {
            Bot.OnMessage += BotOnMessageReceived;
            Bot.OnMessageEdited += BotOnMessageReceived;
            Bot.OnCallbackQuery += BotOnCallbackQueryReceived;

            Bot.StartReceiving(Array.Empty<UpdateType>());
            Console.ReadLine();
            Bot.StopReceiving();
        }
        //public async void BotOnMessageReceivedDo(object sender, MessageEventArgs messageEventArgs)
        //{
        //    await Task.Run(() => BotOnMessageReceived(sender,messageEventArgs));
        //}
        //public async Task GetInlineKeyboardDo(string[] JSONdata, KeyboardData<string>[] data)
        //{
        //    await Task.Run(()=>GetInlineKeyboard(JSONdata, data));
        //}
        //public async void BotOnCallbackQueryReceivedDo(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        //{
        //    await Task.Run(()=>BotOnCallbackQueryReceived(sender,callbackQueryEventArgs));
        //}
        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            if (message == null || message.Type != MessageType.Text) return;
            string mess = message.Text.Split(" ").First();

            switch (mess)
            {
                case "/start":
                    string text1 = "Привіт, будь ласка, введіть e-mail";
                    var User = Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id);
                    if (User == null)
                    {
                        User user = new User()
                        {
                            FirstName = message.Chat.FirstName,
                            LastName = message.Chat.LastName,
                            TelegramId = message.Chat.Id,
                            UserName = message.Chat.Username,
                        };
                        Context.Add<User>(user);
                        Context.SaveChanges();
                    }
                    else
                    {
                        if (User.EMail != null && User.PhoneNumber != null) goto case "/theme";
                        if (Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id).EMail != null) text1 = "Тепер, будь ласка, введіть ваш номер телефону починаючи з +38";
                    }
                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        text: text1,
                        replyMarkup: new ReplyKeyboardRemove());

                    break;

                case "/theme":
                    Theme[] themes = Context.Themes.OrderBy(t => t.Order).ToArray();
                    KeyboardData<string>[] data = new KeyboardData<string>[themes.Length];
                    string[] JSONdata = new string[themes.Length];
                    for (int i = 0; i < themes.Length; i++)
                    {
                        KeyboardData<string> obj = new KeyboardData<string>(CallBackRequestId.Req1, themes[i].Text);
                        data[i] = obj;
                        JSONdata[i] = Newtonsoft.Json.JsonConvert.SerializeObject(data[i]);
                    }
                    var inlineKeyboard = new InlineKeyboardMarkup(GetInlineKeyboard(JSONdata, data));
                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        " тепер ви можете вибрати тему для опитування",
                        replyMarkup: inlineKeyboard);
                    break;
                case "/question":
                    var result = Context.Results.Where(r => r.UserId == message.Chat.Id && r.Text == null).First();
                    result.Text = mess;
                    Context.Update(result);
                    Context.SaveChanges();
                    goto case "/theme";
                //var quest = Context.Questions.Where(q => q.Id == result.QuestionId).First();
                //Context.Themes.Where(t => t.Id == quest.ThemeId).First();
                default:
                    if (Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id) == null) goto case "/start";
                    if (Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id).LastAction == "Запитання:")
                    {
                        var me = Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id);
                        me.LastAction = null;
                        Context.Update(me);
                        Context.SaveChanges();
                        goto case "/question";

                    }
                    if (mess.Contains("@"))
                    {

                        if (Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id).EMail == null)
                        {
                            User user2 = Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id);
                            user2.EMail = mess;
                            Context.Users.Update(user2);
                            Context.SaveChanges();
                        }
                        if (Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id).PhoneNumber != null)
                        {
                            goto case "/theme";
                        }
                        string text2 = "Тепер, будь ласка, введіть ваш номер телефону починаючи з +38";
                        await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        text2,
                        replyMarkup: new ReplyKeyboardRemove());
                        break;
                    };
                    if (mess.Count() == 13 && mess.Contains("+38") )
                    {
                        if(Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id).PhoneNumber == null)
                        { 
                            User user2 = Context.Users.FirstOrDefault(u => u.TelegramId == message.Chat.Id);
                            user2.PhoneNumber = mess;
                            Context.Users.Update(user2);
                            Context.SaveChanges();
                        
                            string text3 = "Дякую за ваші введені дані,";
                            await Bot.SendTextMessageAsync(
                            message.Chat.Id,
                            text3,
                            replyMarkup: new ReplyKeyboardRemove());
                        }
                        goto case "/theme" ;
                    }
                    const string usage = @"/start - Розпочати роботу з ботом";

                    await Bot.SendTextMessageAsync(
                        message.Chat.Id,
                        usage,
                        replyMarkup: new ReplyKeyboardRemove());
                    goto case "/start";  
            }

        }
        private static InlineKeyboardButton[][] GetInlineKeyboard(string[] JSONdata, KeyboardData<string>[] data)
        {
            var keyboardInline = new InlineKeyboardButton[JSONdata.Length][];
            for (var i = 0; i < JSONdata.Length; i++)
            {
                Console.WriteLine(JSONdata[i]);
                InlineKeyboardButton inline = new InlineKeyboardButton
                {
                    Text = data[i].Data,
                    CallbackData = JSONdata[i],
                }; 
                keyboardInline[i] = new InlineKeyboardButton[1];
                keyboardInline[i][0] = inline;
            }
            return keyboardInline;
        }
        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;
            var chatId = callbackQuery.Message.Chat.Id;
            var messId = callbackQuery.Message.MessageId;
            var data = callbackQuery.Data;
            KeyboardData<object> obj = JsonConvert.DeserializeObject<KeyboardData<object>>(data);
            if(obj.rId == CallBackRequestId.Req1)
            {
                Theme theme = Context.Themes.Where(t => t.Text == obj.Data.ToString()).First();
                ResultOfQuestion[] results = Context.Results.Where(r => r.UserId == chatId).ToArray();
                Question[] questions = Context.Questions.Where(q => q.ThemeId == theme.Id).OrderBy(q => q.Order).ToArray();
                for (int i=0;i < results.Length; i++)
                {
                    questions = questions.Where(q => q.Id != results[i].QuestionId).ToArray();
                }
                if (questions.Length != 0)
                {
                    ResultOfQuestion result = new ResultOfQuestion();
                    result.QuestionId = questions[0].Id;
                    result.UserId = chatId;
                    Context.Results.Add(result);
                    Context.SaveChanges();
                    await Bot.SendTextMessageAsync(chatId, "Запитання: "+questions[0].Text, replyMarkup: null);
                    if(questions[0].Type == 2)
                    {
                        Answer[] answers = Context.Answers.Where(a => a.QuestionId == questions[0].Id).OrderBy(a => a.Order).ToArray();
                        KeyboardData<string>[] data2 = new KeyboardData<string>[answers.Length];
                        string[] JSONdata2 = new string[answers.Length];
                        for (int i = 0; i < answers.Length; i++)
                        {
                            KeyboardData<string> obj2 = new KeyboardData<string>(CallBackRequestId.Req3, answers[i].Text);
                            data2[i] = obj2;
                            JSONdata2[i] = Newtonsoft.Json.JsonConvert.SerializeObject(data2[i]);
                        }
                        var inlineKeyboard = new InlineKeyboardMarkup(GetInlineKeyboard(JSONdata2, data2));
                        await Bot.SendTextMessageAsync(
                            chatId,
                            " тепер ви можете вибрати відповідь",
                            replyMarkup: inlineKeyboard);
                    }
                    else { 
                    var me = Context.Users.FirstOrDefault(u => u.TelegramId == chatId);
                    if(me != null) {
                        me.LastAction = "Запитання:";
                        Context.Update(me);
                        Context.SaveChanges();
                        }
                    }
                }
                else
                {
                    await Bot.SendTextMessageAsync(chatId, "Дякуємо за вашу відповідь. Питаннь більше немає", replyMarkup: null);
                }
                return;
                
            }
            if(obj.rId == CallBackRequestId.Req3)
            {
                var result = Context.Results.Where(r => r.UserId == chatId && r.Text == null).First();
                if(result != null)
                {
                    result.Text = obj.Data.ToString();
                    Context.Update(result);
                    Context.SaveChanges();
                }
                await Bot.SendTextMessageAsync(chatId, "Дякуємо за вашу відповідь", replyMarkup: null);
            }
        }

    }
}
