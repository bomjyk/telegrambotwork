﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using Telegram.Bot;

namespace TelegramBot.Services
{
    public interface ITelegramService
    {
        TelegramBotClient _Bot { get; }
        ApplicationContext _Context { get; }
        void SetBot(TelegramBotClient Bot);
        void SetContext(ApplicationContext Context);
        void Main();
    }
}
