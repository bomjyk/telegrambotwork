﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelegramBot.Model
{
    public class Theme
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool Valid { get; set; }
        public int Order { get; set; }
    }
}
