﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TelegramBot.Model
{
    public class ResultOfQuestion
    {
        public int Id { get; set; }
        public long UserId { get; set; }
        public int QuestionId { get; set; }
        public string Text { get; set; }
    }
}
